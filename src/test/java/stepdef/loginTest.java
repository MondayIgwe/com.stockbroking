package stepdef;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class loginTest {
	WebDriver driver = null;
	
	

@Test(priority=0)
@Given("^i am on the browser$")
public void i_am_on_the_browser() throws Throwable {
	System.setProperty("webdriver.chrome.driver","C:\\Users\\Administrator\\workspace\\com.Stcokbroking\\src\\test\\resources\\drivers\\chromedriver.exe");
	  driver = new ChromeDriver();
	
}


@Test(priority=1)
@When("^i enter url")
public void i_enter_url(String url) throws Throwable {
	String URL = url;
	driver.get(URL);
	
}

@Test(priority=2)
@Then("^google opens$")
public void google_opens() throws Throwable {
	System.out.println("The url is:>>>>"+ driver.getCurrentUrl().toString());
	Thread.sleep(500);
	driver.quit();
}

	

}
